# README #

This repository contains Robot Operating System (ROS) library packages. They are catkin packages and use Eigen3. Add to your catkin workspace to build the code.

Packages:

wave_control - A PID controller implementation with gains set through ROS parameters and extra logging features.

wave_estimation - A base implementation for Kalman filters.

wave_utils - Reusable utility functions, mostly math related.